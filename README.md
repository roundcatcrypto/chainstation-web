This is the code repository for the main MoonCatRescue application (UI relaunch). Named "chainstation", it fits into the lore of the MoonCatRescue ecosystem as a front-end interface for interacting with the Ethereum blockchain. It will eventually be deployed as `https://chainstation.mooncatrescue.com`.

# Development
This application is a [NextJS](https://nextjs.org/) application (so, using [React](https://reactjs.org/) and [Typescript](https://www.typescriptlang.org/) under-the-hood), with Web3 integrations added in ([ethers](https://www.npmjs.com/package/ethers) and [web3modal](https://www.npmjs.com/package/web3modal)).

To run a local development environment, follow the general instructions for [MoonCatRescue development](https://gitlab.com/mooncatrescue/dev-environment). The first time you clone this repository, you'll need to run `npm install` in it, to get the needed javascript packages. Then run:

    docker compse up

That will start a running environment you can then access at http://localhost:25602
