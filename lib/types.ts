export interface MoonCatData {
  rescueOrder: number
  catId: string
  genesis: boolean
  pale: boolean
  facing: string
  expression: string
  pattern: string
  pose: string
  rescueYear: number
}
