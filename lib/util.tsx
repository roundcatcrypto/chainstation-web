import React, { useState, useEffect } from 'react'
import { ethers } from 'ethers'

export function sleep(delay: number): Promise<void> {
  return new Promise((resolve) => {
    window.setTimeout(() => {
      resolve()
    }, delay * 1000)
  })
}

/**
 * Given an array of items, add a separator between every one of them.
 * This is a replacement for Array.join() on a list of JSX elements
 */
export function interleave(
  arr: Array<JSX.Element | string>,
  glue: string = ', '
): Array<JSX.Element | String> {
  let formatted: Array<JSX.Element | string> = []
  arr.forEach((field, index) => {
    formatted.push(field)
    if (index < arr.length - 1) formatted.push(glue)
  })
  return formatted
}

/**
 * Custom Hook for transforming a smart contract address into an EthersJS Contract object.
 */
export function useContract(address: string, etherscanKey: string) {
  const [contract, setContract] = useState<ethers.Contract | null>(null)
  const fetchRs = useDelayedFetch(
    'https://api.etherscan.io/api?module=contract&action=getabi&address=' +
      address +
      '&apikey=' +
      etherscanKey
  )
  useEffect(() => {
    if (fetchRs == null) return
    if (!fetchRs.ok) {
      console.error('Etherscan API failure', fetchRs)
      return
    }
    if (fetchRs.data.status != 1) {
      console.error('Etherscan API result failure', fetchRs.data)
      return
    }
    const abi = JSON.parse(fetchRs.data.result)
    console.debug('ABI result', address, abi)
    setContract(new ethers.Contract(address, abi))
  }, [fetchRs])
  return contract
}

interface FetchResult {
  ok: boolean
  data: any
}
/**
 * Custom Hook for fetching an HTTP result, but in an abort-able manner.
 * If the component that uses this hook gets unmounted, an AbortController is
 * used to stop the HTTP request in-flight
 */
export function useDelayedFetch(fetchURI: string) {
  const [fetchResult, setFetchResult] = useState<FetchResult | null>(null)
  useEffect(() => {
    console.log('custom hook mount')
    const controller = new AbortController()
    const signal = controller.signal

    let handle = window.setTimeout(async () => {
      let rs = await fetch(fetchURI, { signal })
      let data = rs.ok ? await rs.json() : null
      setFetchResult({ ok: rs.ok, data })
    })
    return () => {
      console.log('custom hook unmount')
      controller.abort() // Stop fetch in-progress, if it exists
      window.clearTimeout(handle) // Clear any timeout handler, if it exists
    }
  }, [])
  return fetchResult
}
