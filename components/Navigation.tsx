import React, { useContext } from 'react'
import NextLink from 'next/link'
import { ConnectionContext } from 'components/WalletContext'
import HexString from 'components/HexString'

const ZWS = '\u200B'

const Navigation = () => {
  const ctx = useContext(ConnectionContext)

  let button: React.ReactNode, addressView: React.ReactNode
  switch (ctx.state) {
    case 'loading':
      button = <button disabled>Connect</button>
      break
    case 'disconnected':
      button = <button onClick={(e) => ctx.connect()}>Connect</button>
      break
    case 'connected':
      button = <button onClick={(e) => ctx.disconnect()}>Disconnect</button>
      addressView = (
        <div className="address-info">
          <HexString>{ctx.address}</HexString>
        </div>
      )
      break
  }
  const appTitle = 'MoonCat' + ZWS + 'Rescue'
  return (
    <nav>
      <div id="logo">
        <NextLink href="/">{appTitle}</NextLink>
      </div>
      <div style={{ flex: '1 10 auto', overflowY: 'auto' }}>
        <h2>Object Spectrometer</h2>
        <ul>
          <li>
            <NextLink href="/mooncats">MoonCats</NextLink>
          </li>
          <li>
            <NextLink href="/accessories">Accessories</NextLink>
          </li>
          <li>
            <NextLink href="/lootprints">lootprints</NextLink>
          </li>
        </ul>
        <h2>Tools</h2>
        <ul>
          <li>
            <NextLink href="/acclimator">Acclimator</NextLink>
          </li>
          <li>
            <NextLink href="/boutique">Boutique</NextLink>
          </li>
          <li>
            <NextLink href="/mcns">MoonCatNameService</NextLink>
          </li>
        </ul>
        <h2>My Purrse</h2>
      </div>
      <div id="wallet-connection">
        {addressView}
        <div>{button}</div>
      </div>
    </nav>
  )
}
export default Navigation
