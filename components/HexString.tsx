import React from 'react'

interface Props {
  children: string
}

const HexString = ({ children: hex }: Props) => {
  const shortHex = hex.substring(0, 8) + '...' + hex.substring(hex.length - 6)
  return (
    <span title={hex} className="hex-string">
      {shortHex}
    </span>
  )
}
export default HexString
