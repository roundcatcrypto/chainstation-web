import React, { useState, useEffect, useCallback, createContext } from 'react'
import { ethers } from 'ethers'
import WalletConnectProvider from '@walletconnect/web3-provider'
import Web3Modal from 'web3modal'

export type WalletContextLoading = {
  state: 'loading' // Before the app has fully loaded, use this 'empty' state
}
export type WalletContextDisconnected = {
  state: 'disconnected' // Not connected to a user's wallet; using failback public connector
  provider: ethers.providers.BaseProvider
  address: null
  networkId: number
  connect: Function
}
export type WalletContextConnected = {
  state: 'connected' // Connected to a user's wallet
  provider: ethers.providers.BaseProvider
  address: string
  networkId: number
  disconnect: Function
}

export type ConnectionContextProps =
  | WalletContextLoading
  | WalletContextDisconnected
  | WalletContextConnected

export const ConnectionContext = createContext<ConnectionContextProps>({
  state: 'loading',
})

const providerOptions = {
  walletconnect: {
    package: WalletConnectProvider,
    options: {
      rpc: {
        1: 'https://eth-rpc.gateway.pokt.network',
        5: 'https://goerli.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161',
        42161: 'https://arb1.arbitrum.io/rpc',
        421611: 'https://rinkeby.arbitrum.io/rpc',
      },
    },
  },
}

let web3Modal: Web3Modal | undefined
if (typeof window !== 'undefined') {
  web3Modal = new Web3Modal({
    network: 'mainnet',
    cacheProvider: true,
    providerOptions,
  })
}

interface Props {
  children: React.ReactNode
}

const WalletContext = ({ children }: Props) => {
  let [provider, setProvider] = useState<ethers.providers.Web3Provider | null>(
    null
  )
  let [address, setAddress] = useState<string | undefined>(undefined)
  let [networkId, setNetworkId] = useState<number | undefined>(undefined)

  // Do the act of connecting to the user's wallet
  const connect = useCallback(async function () {
    const instance = await web3Modal!.connect()
    const provider = new ethers.providers.Web3Provider(instance, 'any')

    let network = await provider.getNetwork()
    setNetworkId(network.chainId)

    let signer = provider.getSigner()
    let address = await signer.getAddress()
    setAddress(address)

    setProvider(provider)
  }, [])

  const disconnect = useCallback(
    async function () {
      console.debug('disconnecting')
      await web3Modal?.clearCachedProvider()
      if (
        provider?.provider.disconnect &&
        typeof provider.provider.disconnect == 'function'
      ) {
        await provider.provider.disconnect()
      }
      setAddress(undefined)
      setNetworkId(undefined)
      setProvider(null)
    },
    [provider]
  )

  // Auto-connect to cached provider, if present
  useEffect(() => {
    if (web3Modal!.cachedProvider) {
      connect()
    }
  }, [connect])

  // Set up event listeners
  useEffect(() => {
    if (provider == null) return
    console.log('adding listeners', provider.provider)

    // https://docs.ethers.io/v5/concepts/best-practices/#best-practices--network-changes
    const handleChainChanged = (hexChainId: string) => window.location.reload()

    const handleAccountsChanged = (accounts: string[]) => {
      console.debug('accounts changed', accounts)
      setAddress(accounts[0])
    }

    provider.provider.on('accountsChanged', handleAccountsChanged)
    provider.provider.on('chainChanged', handleChainChanged)

    return () => {
      if (provider?.provider.removeListener) {
        provider.provider.removeListener(
          'accountsChanged',
          handleAccountsChanged
        )
        provider.provider.removeListener('chainChanged', handleChainChanged)
      }
    }
  }, [provider])

  let conn: ConnectionContextProps
  if (provider == null || address == null || networkId == null) {
    // Not connected
    let tempProvider = ethers.providers.getDefaultProvider('mainnet', {
      infura: '29a0a3f2ea8a43ea9601e68001fa5468',
    })
    conn = {
      state: 'disconnected',
      provider: tempProvider,
      address: null,
      networkId: 1,
      connect,
    }
  } else {
    // Connected
    conn = { state: 'connected', provider, address, networkId, disconnect }
  }

  return (
    <ConnectionContext.Provider value={conn}>
      {children}
    </ConnectionContext.Provider>
  )
}
export default WalletContext
