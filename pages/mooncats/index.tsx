import React, { useEffect, useState } from 'react'
import type { NextPage } from 'next'
import mooncatTraits from 'lib/mooncat_traits.json'
import { MoonCatData } from 'lib/types'
import Head from 'next/head'

const ZWS = '\u200B'
const API_SERVER_ROOT = 'https://api.mooncat.community'
const PER_PAGE = 50

const MoonCatThumb = ({ moonCat }: { moonCat: MoonCatData }) => {
  return (
    <div className="item-thumb">
      <a href={'/mooncats/' + moonCat.rescueOrder}>
        <div
          className="thumb-img"
          style={{
            backgroundImage:
              'url(' +
              API_SERVER_ROOT +
              '/image/' +
              moonCat.rescueOrder +
              '?scale=3&padding=5)',
          }}
        />
        <p>#{moonCat.rescueOrder}</p>
        <p>
          <code>{moonCat.catId}</code>
        </p>
      </a>
    </div>
  )
}

interface FilterSettings {
  classification?: 'genesis' | 'rescue'
  facing?: 'right' | 'left'
}

const MoonCatsHome: NextPage = () => {
  const [moonCats, setMoonCats] = useState<Array<MoonCatData>>([])
  const [pageNumber, setCurrentPage] = useState<number>(0)
  const [filters, setFilters] = useState<FilterSettings>({})

  function filterMoonCats() {
    let filtered = (mooncatTraits as Array<MoonCatData>).filter((moonCat) => {
      if (typeof filters.classification != 'undefined') {
        if (filters.classification == 'genesis' && moonCat.genesis == false)
          return false
        if (filters.classification == 'rescue' && moonCat.genesis == true)
          return false
      }
      return true
    })
    setMoonCats(filtered)
  }

  useEffect(() => {
    filterMoonCats()
  }, [filters])

  return (
    <div style={{ padding: '1rem' }}>
      <Head>
        <title>MoonCats</title>
        <meta
          name="description"
          content="Browse the entire collection of colorful astral felines"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1 className="hero">MoonCats</h1>
      <div id="filters">
        <button
          onClick={(e) => {
            setFilters((cur) => {
              let filters: FilterSettings = {
                ...cur,
                classification: 'genesis',
              }
              return filters
            })
            setCurrentPage(0)
          }}
        >
          Genesis
        </button>
      </div>
      <div id="item-grid" style={{ margin: '2rem 0' }}>
        {moonCats
          .slice(pageNumber * PER_PAGE, (pageNumber + 1) * PER_PAGE)
          .map((mc) => (
            <MoonCatThumb key={mc.rescueOrder} moonCat={mc} />
          ))}
      </div>
      <div>
        <button
          onClick={(e) => {
            setCurrentPage((curr) => {
              return curr + 1
            })
          }}
        >
          Next
        </button>
      </div>
    </div>
  )
}
export default MoonCatsHome
