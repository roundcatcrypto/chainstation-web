import React from 'react'
import type { NextPage } from 'next'
import Head from 'next/head'
import { MoonCatData } from 'lib/types'

const API_SERVER_ROOT = 'https://api.mooncat.community'

interface Props {
  moonCat: MoonCatData | null
}

function ucFirst(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

const MoonCatDetail: NextPage<Props> = ({ moonCat }) => {
  if (moonCat == null) {
    return (
      <div id="content-container">
        <Head>
          <title>Missing MoonCat</title>
        </Head>
        <h1 className="hero">Missing MoonCat</h1>
        <section className="card">
          No MoonCat with that identifier found...
        </section>
      </div>
    )
  }

  const pageTitle = `MoonCat #${moonCat.rescueOrder} (${moonCat.catId})`

  return (
    <div id="content-container">
      <Head>
        <title>{pageTitle}</title>
      </Head>
      <h1 className="hero">MoonCat #{moonCat.rescueOrder}</h1>
      <section className="card">
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <img
            src={`${API_SERVER_ROOT}/image/${moonCat.catId}`}
            style={{ maxHeight: 250, maxWidth: 600 }}
          />
          <div>
            <ul>
              <li>
                <strong>Rescue Order</strong> #{moonCat.rescueOrder}
              </li>
              <li>
                <strong>Identifier</strong> {moonCat.catId}
              </li>
              <li>
                <strong>Facing</strong> {ucFirst(moonCat.facing)}
              </li>
              <li>
                <strong>Expression</strong> {ucFirst(moonCat.expression)}
              </li>
              <li>
                <strong>Pattern</strong> {ucFirst(moonCat.pattern)}
              </li>
              <li>
                <strong>Pose</strong> {ucFirst(moonCat.pose)}
              </li>
              <li>
                <strong>Rescue Year</strong> {moonCat.rescueYear}
              </li>
            </ul>
          </div>
        </div>
      </section>
    </div>
  )
}

export async function getStaticPaths() {
  const moonCatTraits: Array<MoonCatData> = require('lib/mooncat_traits.json')
  let paths = []
  for (let i = 0; i < moonCatTraits.length; i++) {
    paths.push({
      params: { id: String(moonCatTraits[i].rescueOrder) },
    })
    paths.push({
      params: { id: moonCatTraits[i].catId },
    })
  }
  return {
    paths,
    fallback: false,
  }
}

export async function getStaticProps({ params }) {
  const moonCatTraits: Array<MoonCatData> = require('lib/mooncat_traits.json')
  const { id } = params
  if (Array.isArray(id) || typeof id == 'undefined') {
    console.error('Routing error', id)
    return { props: { moonCat: null } }
  }

  function getById(catId: string) {
    let targetId = catId.toLowerCase()
    for (let i = 0; i < moonCatTraits.length; i++) {
      if (moonCatTraits[i].catId.toLowerCase() == targetId)
        return moonCatTraits[i]
    }
    return null
  }

  if (id.substring(0, 2) == '0x') {
    // ID is a hex ID; look up the MoonCat
    let moonCat = getById(id)
    return { props: { moonCat } }
  } else if (!isNaN(Number(id))) {
    let moonCat = moonCatTraits[Number(id)]
    return { props: { moonCat } }
  }

  return {
    props: { moonCat: null },
  }
}

export default MoonCatDetail
