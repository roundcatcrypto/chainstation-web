import 'styles/globals.scss'
import type { AppProps } from 'next/app'
import BackgroundStars from 'components/BackgroundStars'
import Navigation from 'components/Navigation'
import WalletContext from 'components/WalletContext'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <WalletContext>
      <Navigation />
      <div id="content">
        <Component {...pageProps} />
      </div>
      <BackgroundStars />
    </WalletContext>
  )
}

export default MyApp
