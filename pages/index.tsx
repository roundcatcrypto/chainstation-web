import RandomMoonCat from 'components/RandomMoonCat'
import type { NextPage } from 'next'
import Head from 'next/head'

const ZWS = '\u200B'

interface Props {
}

const Home: NextPage<Props> = ({ }) => {
  const titleString = `MoonCat${ZWS}Rescue Chainstation`
  return (
    <div id="content-container">
      <Head>
        <title>{titleString}</title>
        <meta
          name="description"
          content="Playground of the colorful felines rescued from the moon"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1 className="hero">{titleString}</h1>
      <section className="card-help">
        <p>MoonCats are an original NFT which launched in 2017 and helped pioneer <strong>on-chain generation</strong>, <strong>fair distribution</strong>, and <strong>user customization</strong>. <a href="https://mooncat.community">More info...</a></p>
      </section>
      <div className="mooncat-row">
        <div><RandomMoonCat /></div>
        <div><RandomMoonCat /></div>
        <div><RandomMoonCat /></div>
      </div>
      <section className="card-help">
        <h2>Welcome to the MoonCat{ZWS}Rescue Chainstation!</h2>
        <p>MoonCat{ZWS}Rescue sprouted an amazing <em>grass-roots community</em> of Rescuers, Adopters, Artists, Programmers, and Enthusiasts. <em>People rose to the challenge of reviving a mission half-finished and almost fully forgotten, and saw it through.</em> Though ponderware initially started the MoonCat{ZWS}Rescue mission, a wonderful community has continued to grow around it.</p>
        <p>The MoonCat{ZWS}Rescue team is grateful to have been welcomed into that community. Together, we hope to expand on the world of MoonCat{ZWS}Rescue, adding content and value to the ecosystem as a whole. We hope to <strong>explore, delight, and educate</strong> in the spirit of the original contract.</p>
      </section>
    </div>
  )
}
export default Home
